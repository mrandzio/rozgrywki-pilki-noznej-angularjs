'use strict';

angular.module('myApp').component('confirmDelete', {
    templateUrl: 'components/confirmDelete/confirmDelete.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: ConfirmDeleteCtrl
});

function ConfirmDeleteCtrl($scope) {
    var $ctrl = this;

    $scope.confirm = function () {
        $ctrl.close({ $value: $ctrl.resolve.id });
    };

    $scope.cancel = function () {
        $ctrl.dismiss({ $value: 'cancel' });
    };

}