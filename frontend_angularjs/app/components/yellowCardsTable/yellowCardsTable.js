'use strict';

angular.module('myApp').component('yellowCardsTable', {
    templateUrl: 'components/yellowCardsTable/yellowCardsTable.html',
    controller: YellowCardsTableCtrl,
    css: 'components/yellowCardsTable/yellowCardsTable.css'
});

function YellowCardsTableCtrl($uibModal, $log, $scope, $location, $filter, Players) {

    $scope.getScorers = function () {
        $scope.isDataLoading = true;
        var scorers = [];
        Players.query(function (players) {
            players.forEach(function (scorer, index) {
                scorers[index] = {
                    name: scorer.FirstName + " " + scorer.LastName,
                    team: scorer.Team.Name,
                };
                var goals = 0;
                scorer.Events.forEach(function (event) {
                    if (event.Type === 3) {
                        goals++;
                    }
                });
                scorers[index].goals = goals;
            });
            $scope.isDataLoading = false;
            $scope.scorers = $filter('ranking')(scorers);
        });
    };

    $scope.getScorers();

}