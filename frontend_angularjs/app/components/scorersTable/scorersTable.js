'use strict';

angular.module('myApp').component('scorersTable', {
    templateUrl: 'components/scorersTable/scorersTable.html',
    controller: ScorersTableCtrl,
    css: 'components/scorersTable/scorersTable.css'
});

function ScorersTableCtrl($uibModal, $filter, $scope, $location, Players, $rootScope) {

    $scope.getScorers = function () {
        $scope.isDataLoading = true;
        var scorers = [];
        Players.query(function (players) {
            players.forEach(function (scorer, index) {
                scorers[index] = {
                    name: scorer.FirstName + " " + scorer.LastName,
                    team: scorer.Team.Name,
                };
                var goals = 0;
                scorer.Events.forEach(function (event) {
                    if (event.Type === 0 || event.Type === 1) {
                        goals++;
                    }
                });
                scorers[index].goals = goals;
            });
            $scope.scorers = $filter('ranking')(scorers);
            $scope.isDataLoading = false;
        });
    };

    $scope.getScorers();

}