'use strict';

angular.module('myApp').component('leagueTable', {
    templateUrl: 'components/leagueTable/leagueTable.html',
    controller: LeagueTeamCtrl,
    css: 'components/leagueTable/leagueTable.css'
});

function LeagueTeamCtrl($uibModal, $log, $filter, $scope, $location, Teams, $rootScope) {

    $scope.addAlert = function (type, msg) {
        $rootScope.alerts.push({type: type, msg: msg});
    };

    $scope.getTeams = function () {
        $scope.isDataLoading = true;
        return Teams.query(function (array) {
            angular.forEach(array, function (team) {
                team.Points = 0;
                team.ScoredGoals = 0;
                team.LostGoals = 0;
                team.Lost = 0;
                team.Draw = 0;
                team.Won = 0;

                angular.forEach(team.HostTeamMatches, function (match) {
                    var resultTeam = $filter('resultTeam')(match.Events, team.Id);

                    if (resultTeam.scoredGoals > resultTeam.lostGoals) {
                        team.Points += 3;
                        team.Won++;
                    }
                    else if (resultTeam.scoredGoals === resultTeam.lostGoals) {
                        team.Points++;
                        team.Draw++;
                    }
                    else {
                        team.Lost++;
                    }

                    team.ScoredGoals += resultTeam.scoredGoals;
                    team.LostGoals += resultTeam.lostGoals;
                });

                angular.forEach(team.GuestTeamMatches, function (match) {
                    var scoredGoals = match.Events.filter(x => (x.Player.TeamId === team.Id && (x.Type === 0 || x.Type === 1)) ||
                        (x.Player.TeamId !== team.Id && x.Type === 2)).length;
                    var allGoals = match.Events.filter(x => x.Type !== 3 && x.Type !== 4).length;
                    var lostGoals = allGoals - scoredGoals;

                    if (scoredGoals > lostGoals) {
                        team.Points += 3;
                        team.Won++;
                    }
                    else if (scoredGoals === lostGoals) {
                        team.Points++;
                        team.Draw++;
                    }
                    else {
                        team.Lost++;
                    }

                    team.ScoredGoals += scoredGoals;
                    team.LostGoals += lostGoals;
                });

                team.DifferenceGoals = team.ScoredGoals - team.LostGoals;

            });

            $scope.isDataLoading = false;
        });
    };

    $scope.teams = $scope.getTeams();

    $scope.deleteTeam = function (id) {
        Teams.delete({id: id}, function () {
            $scope.addAlert('success', 'Drużyna została usunięta.');
            $scope.teams = $scope.getTeams();
        });
    };

    $scope.addTeam = function (team) {
        Teams.add(team, function () {
            $scope.addAlert('success', 'Drużyna została dodana.');
            $scope.teams = $scope.getTeams();
        });
    };

    $scope.editTeam = function (team) {
        Teams.edit(team, function () {
            $scope.addAlert('success', 'Drużyna została zmodyfikowana.')
            $scope.teams = $scope.getTeams();
        });
    };

    $scope.detailsTeam = function (id) {
        $location.path('/detailsTeam/' + id);
    };

    $scope.confirmDeleteTeam = function (id) {
        var modalConfirmDeleteInstance = $uibModal.open({
            animation: true,
            component: 'confirmDelete',
            resolve: {
                id: id
            }
        });

        modalConfirmDeleteInstance.result.then(function (id) {
            $scope.deleteTeam(id);
        }, function () {});

    };

    $scope.addTeamPanel = function () {
        var modalFormAddInstance = $uibModal.open({
            animation: true,
            component: 'formTeam',
            resolve: {
                team: {},
                isFormAdd: true
            }
        });

        modalFormAddInstance.result.then(function (team) {
            $scope.addTeam(team);
        }, function () {});

    };

    $scope.editTeamPanel = function (team) {
        var modalFormEditInstance = $uibModal.open({
            animation: true,
            component: 'formTeam',
            resolve: {
                team: team,
                isFormAdd: false
            }
        });

        modalFormEditInstance.result.then(function (team) {
            $scope.editTeam(team);
        }, function () {});

    };

}