'use strict';

angular.module('myApp').component('formEvent', {
    templateUrl: 'components/formEvent/formEvent.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: FormEventCtrl,
    css: 'components/formEvent/formEvent.css'
});

function FormEventCtrl($scope, $filter, $rootScope, $log, Team) {
    const $ctrl = this;

    $ctrl.$onInit = function () {
        $scope.isDataLoading = true;
        $scope.players =[];
        $scope.e =  angular.copy($ctrl.resolve.event);
        $scope.e.Player = null;
        $scope.m =  angular.copy($ctrl.resolve.match);
        $scope.getPlayers($scope.m.HostTeamId, $scope.m.GuestTeamId);
        $scope.isFormAdd = $ctrl.resolve.isFormAdd;
        $scope.typesEvent = $rootScope.typesEvent;
        $scope.matchName = $scope.m.HostTeam.Name + ' - ' + $scope.m.GuestTeam.Name + ' (' + $filter('date')($scope.m.Date, "d.MM.yyyy HH:mm") + ')';
    };

    $scope.getPlayers = function (hid, gid) {
        let playersHostTeam = [];
        let playersGuestTeam = [];
        Team.get({ id: hid }, function (object) {
            playersHostTeam = object.Players;
            Team.get({ id: gid }, function (object) {
                playersGuestTeam = object.Players;
                $scope.players = [...playersHostTeam, ...playersGuestTeam];
                $scope.isDataLoading = false;
            });
        });
    };

    $scope.confirm = function () {
        $ctrl.close({ $value: $scope.e });
    };

    $scope.label = function (i) {
        const teamName = i.TeamId === $scope.m.HostTeamId ? $scope.m.HostTeam.Name : $scope.m.GuestTeam.Name;
        return i.FirstName + " " + i.LastName + ' (' + teamName + ')' ;
    };

    $scope.cancel = function () {
        $ctrl.dismiss();
    };

}