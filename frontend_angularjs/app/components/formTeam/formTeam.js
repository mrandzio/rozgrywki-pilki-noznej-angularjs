'use strict';

angular.module('myApp').component('formTeam', {
    templateUrl: 'components/formTeam/formTeam.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: FormTeamCtrl,
    css: 'components/formTeam/formTeam.css'
});

function FormTeamCtrl($scope) {
    var $ctrl = this;

    $ctrl.$onInit = function () {
        $scope.t =  angular.copy($ctrl.resolve.team);
        $scope.isFormAdd = $ctrl.resolve.isFormAdd;
    };

    $scope.confirm = function () {
        $ctrl.close({ $value: $scope.t });
    };

    $scope.cancel = function () {
        $ctrl.dismiss();
    };

}