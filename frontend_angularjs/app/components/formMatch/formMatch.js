'use strict';

angular.module('myApp').component('formMatch', {
    templateUrl: 'components/formMatch/formMatch.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: FormMatchCtrl,
    css: 'components/formMatch/formMatch.css'
});

function FormMatchCtrl($scope, $log, Teams) {
    var $ctrl = this;

    $ctrl.$onInit = function () {
        $scope.isDataLoading = true;
        $scope.teams = Teams.query(function () {
            $scope.isDataLoading = false;
        });
        $scope.m = angular.copy($ctrl.resolve.match);
        $scope.isFormAdd = $ctrl.resolve.isFormAdd;
        if (!$scope.isFormAdd) {
            $scope.m.GuestTeam = null;
            $scope.m.HostTeam = null;
            $scope.m.Date = new Date($scope.m.Date);
        }
    };

    $scope.confirm = function () {
        $ctrl.close({$value: $scope.m});
    };

    $scope.cancel = function () {
        $ctrl.dismiss();
    };

}