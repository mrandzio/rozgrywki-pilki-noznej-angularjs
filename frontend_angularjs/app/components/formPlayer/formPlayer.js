'use strict';

angular.module('myApp').component('formPlayer', {
    templateUrl: 'components/formPlayer/formPlayer.html',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: FormPlayerCtrl,
    css: 'components/formPlayer/formPlayer.css'
});

function FormPlayerCtrl($scope, $rootScope) {
    var $ctrl = this;

    $ctrl.$onInit = function () {
        $scope.p =  angular.copy($ctrl.resolve.player);
        $scope.t =  angular.copy($ctrl.resolve.team);
        $scope.isFormAdd = $ctrl.resolve.isFormAdd;
        $scope.positions = $rootScope.positions;
    };

    $scope.confirm = function () {
        $ctrl.close({ $value: $scope.p });
    };

    $scope.cancel = function () {
        $ctrl.dismiss();
    };

}