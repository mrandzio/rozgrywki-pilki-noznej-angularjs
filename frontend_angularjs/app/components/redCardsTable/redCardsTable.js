'use strict';

angular.module('myApp').component('redCardsTable', {
    templateUrl: 'components/redCardsTable/redCardsTable.html',
    controller: RedCardsTableCtrl,
    css: 'components/redCardsTable/redCardsTable.css'
});

function RedCardsTableCtrl($uibModal, $filter, $scope, $location, Players) {

    $scope.getScorers = function () {
        $scope.isDataLoading = true;
        var scorers = [];
        Players.query(function (players) {
            players.forEach(function (scorer, index) {
                scorers[index] = {
                    name: scorer.FirstName + " " + scorer.LastName,
                    team: scorer.Team.Name,
                };
                var goals = 0;
                scorer.Events.forEach(function (event) {
                    if (event.Type === 4) {
                        goals++;
                    }
                });
                scorers[index].goals = goals;
            });
            $scope.isDataLoading = false;
            $scope.scorers = $filter('ranking')(scorers);
        });
    };

    $scope.getScorers();

}