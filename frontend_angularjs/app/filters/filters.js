'use strict';

angular.module('myApp')

    .filter('events', function ($filter) {
        return function (inputArray, teamId) {
            var events = inputArray.filter(x => (x.Player.TeamId === Number(teamId) && x.Type !== 2) || (x.Player.TeamId !== Number(teamId) && x.Type === 2));
            return $filter('orderBy')(events, 'Minute');
        };
    })

    .filter('resultTeam', function () {
        return function (inputArray, teamId) {
            var object = {};
            object.scoredGoals = inputArray.filter(x => (x.Player.TeamId === teamId && (x.Type === 0 || x.Type === 1)) ||
                (x.Player.TeamId !== teamId && x.Type === 2)).length;
            object.allGoals = inputArray.filter(x => x.Type !== 3 && x.Type !== 4).length;
            object.lostGoals = object.allGoals - object.scoredGoals;
            return object;
        };
    })

    .filter('ranking', function ($filter) {
        return function (inputArray) {
            var array = inputArray;
            array = $filter('orderBy')(array, ['-goals','name']);
            array = $filter('limitTo')(array, '10');
            array = $filter('filter')(array, {goals: '!0'});
            return array;
        };
    });