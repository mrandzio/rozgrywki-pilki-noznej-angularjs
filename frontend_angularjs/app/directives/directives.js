'use strict';

angular.module('myApp')

    .directive('notEqualMatchTeams', function ($log) {
        return {
            scope: {
                notEqualMatchTeams: '=',
            },
            require: 'ngModel',
            link: function (scope, elem, attr, ngModel) {
                var object = scope.notEqualMatchTeams;

                function checkEqualityTeamsId() {
                    var valid = object.HostTeamId === object.GuestTeamId && object.HostTeamId !== undefined && object.GuestTeamId !== undefined;
                    elem[0].setCustomValidity(valid ? 'Drużyna gospodarza i gości muszą być różne.' : '');
                    ngModel.$setValidity('notEqualMatchTeams', valid);
                }

                scope.$watch('notEqualMatchTeams.HostTeamId', function () {
                    checkEqualityTeamsId();
                });

                scope.$watch('notEqualMatchTeams.GuestTeamId', function () {
                    checkEqualityTeamsId();
                });

                ngModel.$formatters.unshift(function (value) {
                    return value;
                });
            }
        };
    })

    .directive('uniqueNumber', function (){
        return {
            scope: {
                uniqueNumber: '='
            },
            require: 'ngModel',
            link: function(scope, elem, attr, ngModel) {
                var numbers = scope.uniqueNumber;
                var defaultNumber = 0;

                ngModel.$parsers.unshift(function(value) {
                    var valid = numbers.filter(x => x.Number === Number(value) && x.Number !== defaultNumber).length !== 0;
                    elem[0].setCustomValidity(valid ? 'Zawodnik musi mieć unikalny numer w drużynie.' : '');
                    ngModel.$setValidity('uniqueNumber', valid);
                    return value;
                });

                ngModel.$formatters.unshift(function(value) {
                    defaultNumber = Number(value);
                    return value;
                });
            }
        };
    });



