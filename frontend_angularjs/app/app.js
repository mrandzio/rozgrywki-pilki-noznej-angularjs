'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngResource',
    'ui.bootstrap',
    'angularCSS',
    'myApp.mainView',
    'myApp.detailsTeam',
    'myApp.matches',
    'myApp.ranking'
])

    .run(function ($rootScope) {
        $rootScope.alerts = [];
        $rootScope.positions = [
            {
                label: 'Bramkarz',
                value: 0
            },
            {
                label: 'Obrońca',
                value: 1
            },
            {
                label: 'Pomocnik',
                value: 2
            },
            {
                label: 'Napastnik',
                value: 3
            }];
        $rootScope.typesEvent = [
            {
                label: 'Gol',
                value: 0
            },
            {
                label: 'Gol (rzut karny)',
                value: 1
            },
            {
                label: 'Gol (samobój)',
                value: 2
            },
            {
                label: 'Żółta kartka',
                value: 3
            },
            {
                label: 'Czerwona kartka',
                value: 4
            }];
    })

    .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider.otherwise({redirectTo: '/mainView'});
    }])

    .controller('MyAppCtrl', ['$scope', '$log', '$location', '$rootScope', function ($scope, $log, $location, $rootScope) {
        $scope.alerts = $rootScope.alerts;

        $scope.closeAlert = function (index) {
            $rootScope.alerts.splice(index, 1);
        };

        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };
    }]);

