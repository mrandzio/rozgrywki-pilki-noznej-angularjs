'use strict';

angular.module('myApp')

    .factory('Teams', ['$resource', function ($resource) {
        return $resource('http://localhost:64000/Teams/:id', {}, {
            add: {
                method: 'POST',
            },

            edit: {
                method: 'PUT',
            },

            delete: {
                method: 'DELETE',
            }
        });
    }])

    .factory('Players', ['$resource', function ($resource) {
        return $resource('http://localhost:64000/Players/:id', {}, {
            get: {
                method: 'GET',
            }
        });
    }])

    .factory('Team', ['$resource', function ($resource) {
        return $resource('http://localhost:64000/Teams/:id', {}, {
            get: {
                method: 'GET'
            },

            edit: {
                method: 'PUT'
            },

            delete: {
                method: 'DELETE'
            }
        });
    }])

    .factory('Player', ['$resource', function ($resource) {
        return $resource('http://localhost:64000/Players/:id', {}, {
            add: {
                method: 'POST'
            },

            edit: {
                method: 'PUT'
            },

            delete: {
                method: 'DELETE'
            }
        });
    }])

    .factory('Event', ['$resource', function ($resource) {
        return $resource('http://localhost:64000/Events/:id', {}, {
            get: {
                method: 'GET'
            },

            add: {
                method: 'POST'
            },

            edit: {
                method: 'PUT'
            },

            delete: {
                method: 'DELETE'
            }
        });
    }])

    .factory('Matches', ['$resource', function ($resource) {
        return $resource('http://localhost:64000/Matches/:id', {}, {
            add: {
                method: 'POST'
            },

            edit: {
                method: 'PUT'
            },

            delete: {
                method: 'DELETE'
            }
        });
    }]);