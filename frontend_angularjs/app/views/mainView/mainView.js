'use strict';

angular.module('myApp.mainView', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/mainView', {
            templateUrl: 'views/mainView/mainView.html',
            controller: 'MainViewCtrl',
            css: 'views/mainView/mainView.css'
        });
    }])

    .controller('MainViewCtrl', ['$scope', '$routeParams', function($scope, $routeParams) {

    }]);