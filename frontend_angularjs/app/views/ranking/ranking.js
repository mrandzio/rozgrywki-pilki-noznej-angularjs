'use strict';

angular.module('myApp.ranking', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/ranking', {
            templateUrl: 'views/ranking/ranking.html',
            controller: 'RankingCtrl',
            css: 'views/ranking/ranking.css'
        });
    }])

    .controller('RankingCtrl', ['$scope', '$routeParams', function($scope, $routeParams) {

    }]);