'use strict';

angular.module('myApp.detailsTeam', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/detailsTeam/:id/', {
            templateUrl: 'views/detailsTeam/detailsTeam.html',
            controller: 'DetailsTeamCtrl',
            css: 'views/detailsTeam/detailsTeam.css'
        });
    }])

    .controller('DetailsTeamCtrl', ['$routeParams', '$log', '$filter', '$uibModal', '$scope', '$location', 'Team', 'Player', '$rootScope',
        function ($routeParams, $log, $filter, $uibModal, $scope, $location, Team, Player, $rootScope) {

            $scope.typeSort = 'number';

            $scope.$watch('typeSort', function () {
                $scope.sort();
            });

            $scope.sort = function () {
                if ($scope.team.Players !== undefined) {
                    if ($scope.typeSort === 'number')
                        $scope.team.Players = $filter('orderBy')($scope.team.Players, 'Number');
                    else
                        $scope.team.Players = $filter('orderBy')($scope.team.Players, ['Position','FirstName','LastName']);
                }
            };

            $scope.getTeam = function () {
                $scope.isDataLoading = true;
                return Team.get({id: $routeParams.id}, function (array) {
                    $scope.sort();
                    $scope.isDataLoading = false;
                });
            };

            $scope.team = $scope.getTeam();

            $scope.backToMainView = function () {
                $location.path('/mainView/');
            };

            $scope.addAlert = function (type, msg) {
                $rootScope.alerts.push({type: type, msg: msg});
            };

            $scope.deleteTeam = function (id) {
                Team.delete({id: id}, function () {
                    $scope.addAlert('success', 'Drużyna została usunięta.');
                    $scope.backToMainView();
                });
            };

            $scope.editTeam = function (team) {
                Team.edit(team, function () {
                    $scope.addAlert('success', 'Drużyna została zmodyfikowana.');
                    $scope.team = $scope.getTeam();
                });
            };

            $scope.confirmDeleteTeam = function (id) {
                var modalConfirmDeleteInstance = $uibModal.open({
                    animation: true,
                    component: 'confirmDelete',
                    resolve: {
                        id: id
                    }
                });

                modalConfirmDeleteInstance.result.then(function (id) {
                    $scope.deleteTeam(id);
                }, function () {
                });
            };

            $scope.editTeamPanel = function (team) {
                var modalFormEditInstance = $uibModal.open({
                    animation: true,
                    component: 'formTeam',
                    resolve: {
                        team: team,
                        isFormAdd: false
                    }
                });

                modalFormEditInstance.result.then(function (team) {
                    $scope.editTeam(team);
                }, function () {
                });
            };

            $scope.deletePlayer = function (id) {
                Player.delete({id: id}, function () {
                    $scope.addAlert('success', 'Zawodnik został usunięty.');
                    $scope.team = $scope.getTeam();
                });
            };

            $scope.editPlayer = function (player) {
                Player.edit(player, function () {
                    $scope.addAlert('success', 'Zawodnik został zmodyfikowany.');
                    $scope.team = $scope.getTeam();
                });
            };

            $scope.addPlayer = function (player) {
                Player.add(player, function () {
                    $scope.addAlert('success', 'Zawodnik został dodany.');
                    $scope.team = $scope.getTeam();
                });
            };

            $scope.confirmDeletePlayer = function (id) {
                var modalConfirmDeleteInstance = $uibModal.open({
                    animation: true,
                    component: 'confirmDelete',
                    resolve: {
                        id: id
                    }
                });

                modalConfirmDeleteInstance.result.then(function (id) {
                    $scope.deletePlayer(id);
                }, function () {
                });
            };

            $scope.editPlayerPanel = function (player, team) {
                var modalFormEditPlayerInstance = $uibModal.open({
                    animation: true,
                    component: 'formPlayer',
                    resolve: {
                        team: team,
                        player: player,
                        isFormAdd: false
                    }
                });

                modalFormEditPlayerInstance.result.then(function (player) {
                    $scope.editPlayer(player);
                }, function () {
                });

            };

            $scope.addPlayerPanel = function (team) {
                var modalFormAddPlayerInstance = $uibModal.open({
                    animation: true,
                    component: 'formPlayer',
                    resolve: {
                        team: team,
                        player: {
                            TeamId: team.Id
                        },
                        isFormAdd: true
                    }
                });

                modalFormAddPlayerInstance.result.then(function (player) {
                    $scope.addPlayer(player);
                }, function () {
                });

            };

            $scope.findPosition = function (value) {
                return $filter("filter")($scope.positions, {value: value})[0].label;
            };

            $scope.positions = $rootScope.positions;

        }]);

