'use strict';

angular.module('myApp.matches', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/matches', {
            templateUrl: 'views/matches/matches.html',
            controller: 'MatchesCtrl',
            css: 'views/matches/matches.css'
        });
    }])

    .controller('MatchesCtrl', ['$routeParams', '$log', '$filter', '$uibModal', '$scope', '$location', 'Matches', 'Event', '$rootScope',
        function ($routeParams, $log, $filter, $uibModal, $scope, $location, Matches, Event, $rootScope) {

            $scope.getMatches = function () {
                $scope.isDataLoading = true;
                return Matches.query(function (array) {
                    angular.forEach(array, function(match) {
                        match.GuestTeam.Events = $filter('events')(match.Events, match.GuestTeamId);
                        match.HostTeam.Events = $filter('events')(match.Events, match.HostTeamId);
                        match.GuestTeam.Goals = match.GuestTeam.Events.filter(x => x.Type < 3).length;
                        match.HostTeam.Goals = match.HostTeam.Events.filter(x => x.Type < 3).length;
                    });
                    $scope.isDataLoading = false;
                });
            };

            $scope.matches = $scope.getMatches();

            $scope.addAlert = function (type, msg) {
                $rootScope.alerts.push({type: type, msg: msg});
            };

            $scope.deleteMatch = function (id) {
                Matches.delete({id: id}, function () {
                    $scope.addAlert('success', 'Mecz został usunięty.');
                    $scope.matches = $scope.getMatches();
                });
            };

            $scope.editMatch = function (match) {
                Matches.edit(match, function () {
                    $scope.addAlert('success', 'Mecz został zmodyfikowany.');
                    $scope.matches = $scope.getMatches();
                });
            };

            $scope.confirmDeleteMatch = function (id) {
                var modalConfirmDeleteInstance = $uibModal.open({
                    animation: true,
                    component: 'confirmDelete',
                    resolve: {
                        id: id
                    }
                });

                modalConfirmDeleteInstance.result.then(function (id) {
                    $scope.deleteMatch(id);
                }, function () {});
            };

            $scope.editMatchPanel = function (match) {
                var modalFormEditMatchInstance = $uibModal.open({
                    animation: true,
                    component: 'formMatch',
                    resolve: {
                        match: match,
                        isFormAdd: false
                    }
                });

                modalFormEditMatchInstance.result.then(function (match) {
                    match.Date = new Date($filter('date')(match.Date, "yyyy-MM-dd HH:mm UTC"));
                    $scope.editMatch(match);
                }, function () {});
            };

            $scope.addAlert = function (type, msg) {
                $rootScope.alerts.push({type: type, msg: msg});
            };

            $scope.addMatch = function (match) {
                Matches.add(match, function () {
                    $scope.addAlert('success', 'Mecz został dodany.');
                    $scope.matches = $scope.getMatches();
                });
            };

            $scope.addMatchPanel = function () {
                var modalFormAddMatchInstance = $uibModal.open({
                    animation: true,
                    component: 'formMatch',
                    resolve: {
                        match: {},
                        isFormAdd: true
                    }
                });

                modalFormAddMatchInstance.result.then(function (match) {
                    match.Date = new Date($filter('date')(match.Date, "yyyy-MM-dd HH:mm UTC"));
                    $scope.addMatch(match);
                }, function () {
                });
            };

            $scope.addEventPanel = function (match) {
                var modalFormAddEventInstance = $uibModal.open({
                    animation: true,
                    component: 'formEvent',
                    resolve: {
                        match: match,
                        event: {
                            MatchId: match.Id
                        },
                        isFormAdd: true
                    }
                });

                modalFormAddEventInstance.result.then(function (event) {
                    $scope.addEvent(event);
                }, function () {
                });
            };

            $scope.addEvent = function (event) {
                Event.add(event, function () {
                    $scope.addAlert('success', 'Wydarzenie zostało dodane.');
                    $scope.matches = $scope.getMatches();
                });
            };

            $scope.confirmDeleteEvent = function (id) {
                var modalConfirmDeleteInstance = $uibModal.open({
                    animation: true,
                    component: 'confirmDelete',
                    resolve: {
                        id: id
                    }
                });

                modalConfirmDeleteInstance.result.then(function (id) {
                    $scope.deleteEvent(id);
                }, function () {});
            };

            $scope.deleteEvent = function (id) {
                Event.delete({id: id}, function () {
                    $scope.addAlert('success', 'Wydarzenie zostało usunięte.');
                    $scope.matches = $scope.getMatches();
                });
            };

            $scope.editEventPanel = function (match, event) {
                var modalFormEditEventInstance = $uibModal.open({
                    animation: true,
                    component: 'formEvent',
                    resolve: {
                        match: match,
                        event: event,
                        isFormAdd: false
                    }
                });

                modalFormEditEventInstance.result.then(function (event) {
                    $scope.editEvent(event);
                }, function () {
                });
            };

            $scope.editEvent = function (event) {
                Event.edit(event, function () {
                    $scope.addAlert('success', 'Wydarzenie zostało zmodyfikowane.');
                    $scope.matches = $scope.getMatches();
                });
            };

        }]);

