﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using backend.Models.EFModels;

namespace backend.Controllers
{
    public class MatchesController : ApiController
    {
        private FootballEntities db = new FootballEntities();

        // GET: Matches
        public IQueryable<Match> GetMatch()
        {
            return db.Match.OrderByDescending(m => m.Date);
        }

        // GET: Matches/5
        [ResponseType(typeof(Match))]
        public IHttpActionResult GetMatch(int id)
        {
            Match match = db.Match.Find(id);
            if (match == null)
            {
                return NotFound();
            }

            return Ok(match);
        }

        // PUT: Matches/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMatch(Match match)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var eventsToRemove = db.Event.Where(x => x.MatchId == match.Id && x.Player.TeamId != match.HostTeamId && x.Player.TeamId != match.GuestTeamId);

            foreach (var _event in eventsToRemove.ToList())
            {
                db.Event.Remove(_event);
            }

            match.Events = null;

            db.Entry(match).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MatchExists(match.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: Matches
        [ResponseType(typeof(Match))]
        public IHttpActionResult PostMatch(Match match)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Match.Add(match);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = match.Id }, match);
        }

        // DELETE: Matches/5
        [ResponseType(typeof(Match))]
        public IHttpActionResult DeleteMatch(int id)
        {
            Match match = db.Match.Find(id);
            if (match == null)
            {
                return NotFound();
            }

            foreach (var _event in match.Events.ToList())
                db.Event.Remove(_event);

            db.Match.Remove(match);
            db.SaveChanges();

            return Ok(match);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MatchExists(int id)
        {
            return db.Match.Count(e => e.Id == id) > 0;
        }
    }
}