﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using backend.Models.EFModels;

namespace backend.Controllers
{
    public class TeamsController : ApiController
    {
        private FootballEntities db = new FootballEntities();

        // GET: Teams
        public IQueryable<Team> GetTeam()
        {
            return db.Team;
        }

        // GET: Teams/5
        [ResponseType(typeof(Team))]
        public IHttpActionResult GetTeam(int id)
        {
            Team team = db.Team.Find(id);
            if (team == null)
            {
                return NotFound();
            }

            return Ok(team);
        }

        // PUT: Teams/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTeam(Team team)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            team.Players = null;
            team.GuestTeamMatches = null;
            team.HostTeamMatches = null;

            db.Entry(team).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TeamExists(team.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: Teams
        [ResponseType(typeof(Team))]
        public IHttpActionResult PostTeam(Team team)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Team.Add(team);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = team.Id }, team);
        }

        // DELETE: Teams/5
        [ResponseType(typeof(Team))]
        public IHttpActionResult DeleteTeam(int id)
        {
            Team team = db.Team.Find(id);
            if (team == null)
            {
                return NotFound();
            }

            foreach (var match in team.GuestTeamMatches.ToList())
            {
                foreach (var _event in match.Events.ToList())
                    db.Event.Remove(_event);
                db.Match.Remove(match);
            }
                
            foreach (var match in team.HostTeamMatches.ToList())
            {
                foreach (var _event in match.Events.ToList())
                    db.Event.Remove(_event);
                db.Match.Remove(match);
            }

            foreach (var player in team.Players.ToList())
                db.Player.Remove(player);

            db.Team.Remove(team);
            db.SaveChanges();

            return Ok(team);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TeamExists(int id)
        {
            return db.Team.Count(e => e.Id == id) > 0;
        }
    }
}